set dotcoverpath="C:\Users\appveyor\AppData\Local\JetBrains\Installations\dotCover05\"

cd %dotcoverpath%
dotcover.exe analyse /TargetExecutable="C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\mstest.exe" /TargetArguments="/testcontainer:C:\projects\samplerepositoryforappveyor\Eisk.MVC.Tests\bin\Debug\Eisk.Tests.dll" /Output="DotcoverReport.html" /ReportType="HTML"

set projectpath="C:\projects\samplerepositoryforappveyor"
cd %projectpath%