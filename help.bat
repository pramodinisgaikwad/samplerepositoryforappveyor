rem csc -help
rem set pathMSBuild="C:\Program Files (x86)\MSBuild\12.0\Bin\"
@echo off
cls


rem set sonarpath="C:\ProgramData\chocolatey\lib\msbuild-sonarqube-runner\tools\"
rem cd %sonarpath%



rem for sonar runner execution
set sonarpath="C:\projects\samplerepositoryforappveyor"
rem cd %sonarpath%
rem "C:\projects\samplerepositoryforappveyor\sonar-runner-2.4\bin\sonar-runner.bat" 

rem for run msbuild sonare runner from choclatey package
rem "C:\ProgramData\chocolatey\lib\msbuild-sonarqube-runner\tools\MSBuild.SonarQube.Runner.exe" begin /k:"Varsha" /n:"Eisk" /v:"1.0"


rem IF "%APPVEYOR_REPO_BRANCH%"=="master" (
rem    echo Run Code analysi with the key master and mode publish
rem   "C:\projects\samplerepositoryforappveyor\MSBuild.SonarQube.Runner-2.0\MSBuild.SonarQube.Runner.exe" begin /k:%APPVEYOR_REPO_BRANCH% /n:"Eisk MVC" /v:"1.0" /d:sonar.analysis.mode="publish"
rem    )
    
rem IF "%APPVEYOR_REPO_BRANCH%"=="feature" (
rem    echo Run Code analysi with the key feature and mode incremental
rem   "C:\projects\samplerepositoryforappveyor\MSBuild.SonarQube.Runner-2.0\MSBuild.SonarQube.Runner.exe" begin /k:%APPVEYOR_REPO_BRANCH% /n:"Eisk MVC" /v:"1.0" /d:sonar.analysis.mode="issues" /d:sonar.issuesReport.html.enable="true"
rem    )


rem for run msbuild sonare runner from bitbucket package
rem "C:\projects\samplerepositoryforappveyor\MSBuild.SonarQube.Runner-2.0\MSBuild.SonarQube.Runner.exe" begin /k:"master" /n:"Eisk MVC" /v:"1.0" /d:sonar.analysis.mode="publish"

rem cd %pathMSBuild%
"C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe" "C:\projects\samplerepositoryforappveyor\Eisk.MVC-VS2012.sln" /p:configuration=debug

rem to create package in zip
rem "C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe" "C:\projects\samplerepositoryforappveyor\Eisk.MVC\Bin" /t:Package /p:PackageLocation=EISK.zip

rem "C:\projects\samplerepositoryforappveyor\MSBuild.SonarQube.Runner-2.0\MSBuild.SonarQube.Runner.exe" end

rem dotcover execution
rem "C:\Users\appveyor\AppData\Local\JetBrains\Installations\dotCover05\dotcover.exe" analyse /TargetExecutable="C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\mstest.exe" /TargetArguments="/testcontainer:C:\projects\samplerepositoryforappveyor\Eisk.MVC.Tests\bin\Debug\Eisk.Tests.dll" /Output="AppCoverageReport.html" /ReportType="HTML"
rem "C:\Users\appveyor\AppData\Local\JetBrains\Installations\dotCover05\dotcover.exe" analyse /TargetExecutable="C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\mstest.exe" /TargetArguments="/testcontainer:C:\projects\samplerepositoryforappveyor\Eisk.MVC.Tests\bin\Debug\Eisk.Tests.dll" /Output="Appveyor.html" /ReportType="HTML"

rem rem set nugetpath="C:\projects\samplerepositoryforappveyor\"
rem cd %nugetpath%
rem appveyor PushArtifact Eisk.MVC.5.6.100.0.nupkg

rem set zippath="C:\projects\samplerepositoryforappveyor\Eisk.MVC\bin"
rem cd %zippath%
rem appveyor PushArtifact Eisk.MVC.zip